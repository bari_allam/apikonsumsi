'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var participants = new Schema({
    identity_ID: String,
    full_name : String,
    gender : String,
    address : String,
    photo: String,
    contingent_id : String,
    event_id : String,
    height: Number,
    weight : Number,
    ID_Number : String,
    Family_Number : String,
    insurance_Number : String,
    Register_Number : String,
    insurance_place : String,
    registered :{
        register_date : Date,
        register_by : String
    },
    participant_status : Boolean,
    participant_type : String
});

module.exports = mongoose.model('participants', participants);