'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var entrie_by_numbers = new Schema({
    contingent_id: String,
    category : Array,
    event_id : String,
    entrie_date : Date,
    entrie_by : String,
    change_number : {
        type: Number,
        default : 0
    }
});

module.exports = mongoose.model('entrie_by_numbers', entrie_by_numbers);