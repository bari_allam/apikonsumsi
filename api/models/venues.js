'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var venues = new Schema({
    name: String,
    capasity : Number,
    picture : String,
    location : {
        latitude : String,
        longitude : String
    }
});

module.exports = mongoose.model('venues', venues);