'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var discipline = new Schema({
    name: String,
    sport : String,
    category : [{
        name: String,
        gender : String,
        team_category : Boolean,
        rules :{
            quota : Number,
            age_limit_min : Number,
            age_limit_max : Number 
        },
        game_type : String
    }]
});

module.exports = mongoose.model('discipline', discipline);