'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var entrie_by_team = new Schema({
    contingent_id: String,
    event_id : String,
    gender : String,
    athlete : {
        men : Number,
        women : Number
    },
    official : {
        men : Number,
        women : Number
    }
});

module.exports = mongoose.model('entrie_by_team', entrie_by_team);