'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var entrie_athletes = new Schema({
    event_id : String,
    contingent_id: String,
    participant_id : String,
    categories : Array,
    position : String,
    register : {
        register_date : Date,
        register_by : String
    },
    register_status : Boolean
});

module.exports = mongoose.model('entrie_athletes', entrie_athletes);