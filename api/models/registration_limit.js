'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var registration_limit = new Schema({
    category_id: String,
    event_id : String,
    type :String,
    value :{
        min :Number,
        max : Number
    },
});

module.exports = mongoose.model('registration_limit', registration_limit);