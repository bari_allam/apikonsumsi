'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var entrie_officials = new Schema({
    event_id : String,
    contingent_id: String,
    participant_id : String,
    position : String,
    registerd : {
        register_date : Date,
        register_by : String
    },
    register_status : Boolean
});

module.exports = mongoose.model('entrie_officials', entrie_officials);