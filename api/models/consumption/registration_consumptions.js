'use-strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var registration_consumptions = new Schema({
    date: Date,
    sport: String,
    contingents: String,
    committees: String,
    athlete: Number,
    official: Number
})

module.exports = mongoose.model('registration_consumption', registration_consumptions);