'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var contingents = new Schema({
    name: String,
    initial : String,
    logo : String
});

module.exports = mongoose.model('contingents', contingents);