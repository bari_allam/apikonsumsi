'use-strict'
var mongoose=require('mongoose');
var Schema = mongoose.Schema;

var catering_vendors = new Schema({
    name: String,
    address: String,
    pic_name: String,
    pic_contact: String,
    vendor_photo: String,
    vendor_banner: String,
    cluster: String,
    menu:[{
        foods_name: String,
        foods_photo: String,
        foods_price: Number,
        foods_desc: String,
        foods_type: String,
        quantity: {type: Number, default: 0}
    }]
})

module.exports = mongoose.model('catering_vendors', catering_vendors);