'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var hotel_vendors = new Schema({
    hotel_name : String,
    hotel_address : String,
    hotel_contact: String,
    hotel_image: String
});

module.exports = mongoose.model('hotel_vendors', hotel_vendors);