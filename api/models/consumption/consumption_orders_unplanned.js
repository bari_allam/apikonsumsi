'use-strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var consumption_order_unplanned = new Schema({
    date: Date,
    date_booked: String,
    committee_id: {type: Schema.Types.ObjectId, ref: 'committees'},
    // comitee_id: String,
    vendor: {type: Schema.Types.ObjectId, ref: 'catering_vendors'},
    // vendor_name: String,
    
    menu:[{
        // menu_id: {type: Schema.Types.ObjectId, ref:'catering_vendors.menu'},
        menu_id:String,
        consumption_type: String,
        serves_type: String,
        quantity: Number,
        total_price: Number
    }],
    order_status: String,
    create_date: Date,
    created_by: String,
    created_for: String,
    cluster: String,
})
module.exports = mongoose.model('consumption_order_unplanned', consumption_order_unplanned);