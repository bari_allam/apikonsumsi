'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var sport = new Schema({
    name: String,
    logo : String
});

module.exports = mongoose.model('sport', sport);