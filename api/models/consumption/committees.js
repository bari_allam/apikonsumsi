
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var committees = new Schema({
    name: String,
    logo: String,
    category: String,
})

module.exports = mongoose.model('committees', committees);