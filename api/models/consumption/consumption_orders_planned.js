'use-strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var consumption_orders_planned = new Schema ({
    dateBooked: Date, // Tanggal Konsumsi
    // "Mutation"

    //Penerima Konsumsi 
    /*1. Kontingen
      2. Panpel Cabor
      3. Panitia Bidang Lainnya    */

    consumer :  String, //Kontingen, Panpel Cabor, Panitia Bidang Lainnya    
    quantity : Number, //Number of consumtion order
    consumerDetail : [{
        // detailId : String,
        detail : String,
        qty : Number
    }], 
    cluster: String,
    
    committees: {type: Schema.Types.ObjectId, ref: 'committees'},
    //sport: {type: Schema.Types.ObjectId, ref: 'sport'}, 
    event: {type:Schema.Types.ObjectId, ref: 'event'},
    //contingent: {type: Schema.Types.ObjectId, ref: 'contingents'},

    // vendor_pagi: {type: Schema.Types.ObjectId, ref: 'catering_vendors'},
    // vendor_siang: {type: Schema.Types.ObjectId, ref: 'catering_vendors'},
    // vendor_malam: {type: Schema.Types.ObjectId, ref: 'catering_vendors'},
    // venue_pagi: {type: Schema.Types.ObjectId, ref: 'destinations'},
    // venue_siang: {type: Schema.Types.ObjectId, ref: 'destinations'},
    // venue_malam: {type: Schema.Types.ObjectId, ref: 'destinations'},

    menu: [{
      serving: String, //Jenis Penyajian
      orderType: String, //Waktu Konsumsi (Makan Pagi, Makan Siang, Makan Malam)
      orderStatus: String, //Status Konsumsi
      destination : String,   //Tujuan Pengantaran
      consumptionType: String, 
      quantity: {type: Number, default: 0},
      notes: String,
      vendor: String
    }],
    
    // create_date: Date, 
    createdBy: String,
})

module.exports = mongoose.model('consumption_orders_planned', consumption_orders_planned);