'use-strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var destinations = new Schema({
    name: String,
    address: String,
    type : String,
    photo: String
})

module.exports = mongoose.model('destinations', destinations);
