'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var event = new Schema({
    event_name: String,
    event_icon : String,
    cluster: String,
    event_categories :[{
        category_name : String,
        category_gender : String,
        category_team : Boolean,
        category_team_number : Number
    }],
    event_location : Array,
    event_schedules : {
        match_schedules : Array,
        arrival_schedules : Array,
        departure_schedules : Array,
        technical_meeting_schedules : Array,
        start_date : Date,
        end_date : Date
    },
    sport_id : String,
    discipline_id : String,
    event_parent : String,
});

module.exports = mongoose.model('event', event);