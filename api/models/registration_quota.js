'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var registration_quota = new Schema({
    category_id: String,
    event_id : String,
    athlete :{
        min :Number,
        max : Number
    },
    official :{
        min :Number,
        max : Number
    },
});

module.exports = mongoose.model('registration_quota', registration_quota);