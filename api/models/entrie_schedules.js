'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var entrie_schedules = new Schema({
    event_name: String,
    schedules :{
        entrie_1 : {startdate : Date, enddate : Date},
        entrie_2 : {startdate : Date, enddate : Date},
        entrie_3 : {startdate : Date, enddate : Date},
        accreditation : {startdate : Date, enddate : Date},
    }
});

module.exports = mongoose.model('entrie_schedules', entrie_schedules);