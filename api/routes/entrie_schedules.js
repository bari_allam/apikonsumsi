const express = require('express');
const router = express.Router();
const entrie_schedules = require('../controllers/entrie_schedules');
router.get('/', entrie_schedules.get_all);
router.get('/:name', entrie_schedules.read_data);
router.post('/', entrie_schedules.create_data);
router.put('/:_id', entrie_schedules.update_data);
module.exports = router;