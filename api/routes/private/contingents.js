const express = require('express');
const router = express.Router();
const contingents = require('../../controllers/contingents');
router.get('/', contingents.get_all);
router.get('/:_id', contingents.read_data);
router.post('/create', contingents.create_data);
router.put('/:_id', contingents.update_data);
router.delete('/:_id', contingents.delete_data);
module.exports = router;