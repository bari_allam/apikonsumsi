const express = require('express');
const router = express.Router();
const venues = require('../../controllers/venues');
router.get('/', venues.get_all);
router.get('/:_id', venues.read_data);
router.post('/create', venues.create_data);
router.put('/:_id', venues.update_data);
router.delete('/:_id', venues.delete_data);
module.exports = router;