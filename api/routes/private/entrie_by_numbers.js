const express = require('express');
const router = express.Router();
const entrie = require('../../controllers/entrie_by_numbers');
router.get('/', entrie.get_all);
router.get('/:_id', entrie.read_data);
router.get('/contingent/:_id', entrie.read_data_by_contingent);
router.post('/create', entrie.create_data);
router.post('/createMany', entrie.create_data_multiple);
router.put('/:_id', entrie.update_data);
router.delete('/:_id', entrie.delete_data);
module.exports = router;