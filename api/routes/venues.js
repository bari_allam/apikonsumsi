const express = require('express');
const router = express.Router();
const venues = require('../controllers/venues');
router.get('/', venues.get_all);
router.get('/:_id', venues.read_data);
module.exports = router;