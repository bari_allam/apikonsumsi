const express = require('express');
const router = express.Router();
const events = require('../controllers/events');
router.get('/', events.get_all);
router.get('/:_id', events.read_data);
module.exports = router;