const express = require('express');
const router = express.Router();
const hotel_vendors = require('../controllers/hotel_vendors');
router.get('/', hotel_vendors.get_all);
router.get('/:_id', hotel_vendors.read_data);
router.post('/create', hotel_vendors.create_data);
router.post('/createMany', hotel_vendors.create_data_multiple);
router.put('/:_id', hotel_vendors.update_data);
router.delete('/:_id', hotel_vendors.delete_data);
module.exports = router;