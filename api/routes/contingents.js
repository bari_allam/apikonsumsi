const express = require('express');
const router = express.Router();
const teamControllers = require('../controllers/contingents');
router.get('/', teamControllers.get_all);
router.get('/:_id', teamControllers.read_data);

// Added
router.post('/create', teamControllers.create_data);
router.post('/createMany', teamControllers.create_data_multiple);
router.put('/:_id', teamControllers.update_data);
router.delete('/:_id', teamControllers.delete_data);
module.exports = router;