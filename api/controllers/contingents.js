'use strict';
var teams = require("../models/contingents");
// var teams = require("../models/consumption/contingents");
const multer = require("multer");

// Upload Image
const storage = multer.diskStorage({
    destination: "./repo/contingent/",
    filename: function(req, file, cb) {
      cb(null, "user-" + file.originalname);
    }
  });
  
const upload = multer({
    storage: storage,
    limits: { fileSize: 1000000 },
}).single("logo");


exports.get_all = function(req, res, next) {
    var {_id}= req.query;
    var query ={}
    if(_id){
        query = {...query, _id}
    }
    teams.find(query, function(err, data) {
        if (err){
            next(err);
        }
        else{
            res.json(data);
        }
    }).sort({name:1});
};

exports.create_data = function(req, res, next) {

    upload(req, res, err => {
        var new_data = {
            name: req.body.name,
            initial: req.body.initial
        };
        if (req.file != null) {
          Object.assign(new_data, { logo: req.file.filename });
        }
        var new_post = new teams(new_data);
        if (err) {
           next(err);
        } else {
            new_post.save(function(err, data) {
                if (err){
                    next(err);}
                else{
                    res.json(data);
                }
            });
        }
    });
};

// Create Many
exports.create_data_multiple = function(req, res, next) {
    teams.insertMany(req.body, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};

exports.read_data = function(req, res, next) {
    teams.find({_id:req.params._id}, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};


exports.update_data = function(req, res, next) {
    upload(req, res, err => {
        var new_data = {
            name: req.body.name,
            initial: req.body.initial
        };
        if (req.file != null) {
            Object.assign(new_data, { logo: req.file.filename });
        }
        if (err) {
           next(err);
        } else {
           teams.findOneAndUpdate({_id: req.params._id}, new_data, {new: true}, function(err, data) {
              if (err){
                 next(err);}
              else{
                 res.json(data);
              }
           });
        }
    });
};

exports.delete_data = function(req, res, next) {
    teams.remove({
        _id: req.params._id
    }, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json({ message: 'successfully deleted', data : data });
        }
    });
};