'use strict';
// var registration_consumptions = require("../models/registration_consumptions");
var registration_consumptions = require("../models/consumption/registration_consumptions");

exports.get_all = function(req, res, next) {
    var {_id}= req.query;
    var query ={}
    if(_id){
        query = {...query, _id}
    }
    registration_consumptions.find(query, function(err, result) {
        if (err){
            next(err);
        }
        else{
            res.json(result);
        }
    });
};

exports.create_data = function(req, res, next) {
    var new_data = new registration_consumptions(req.body);
    new_data.save(function(err, result) {
        if (err){
            next(err);}
        else{
        res.json(result);
        }
    });
};

exports.create_data_multiple = function(req, res, next) {
    registration_consumptions.insertMany(req.body, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.read_data = function(req, res, next) {
    registration_consumptions.find({_id:req.params._id}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.update_data = function(req, res, next) {
    registration_consumptions.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.delete_data = function(req, res, next) {
    registration_consumptions.remove({
        _id: req.params._id
    }, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json({ message: 'successfully deleted', result : result });
        }
    });
};


// Aggregate Here
exports.get_registration = function(req, res, next) {    
    registration_consumptions.aggregate([
        {
            "$addFields": {
                "registration_id_ref":{
                    "$toString": "$_id"
                }
            }
        },
        {
            // Contingents
            "$lookup": {
                "from": "contingents",
                "localField": "registration_id_ref",
                "foreignField": "_id",
                "as": "data_contingents"
            },

            // Sports
            "$lookup":{
                "from": "sports",
                "localField": "sports",
                "foreignField": "_id",
                "as": "data_sports"
            }
        },
        {
            "$sort": {
                "date": 1
            }
        }
    ], function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};