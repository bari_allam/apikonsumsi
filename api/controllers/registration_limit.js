'use strict';
var registration_limit = require("../models/registration_limit");

exports.get_all = function(req, res, next) {
    registration_limit.find({}, function(err, result) {
        if (err){
            next(err);
        }
        else{
            res.json(result);
        }
    });
};

exports.create_data = function(req, res, next) {
    var new_data = new registration_limit(req.body);
    new_data.save(function(err, result) {
        if (err){
            next(err);}
        else{
        res.json(result);
        }
    });
};

exports.create_data_multiple = function(req, res, next) {
    registration_limit.insertMany(req.body, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.read_data = function(req, res, next) {
    registration_limit.find({_id:req.params._id}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.update_data = function(req, res, next) {
    registration_limit.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.delete_data = function(req, res, next) {
    registration_limit.remove({
        _id: req.params._id
    }, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json({ message: 'successfully deleted', result : result });
        }
    });
};