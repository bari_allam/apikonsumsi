'use strict';
var venues = require("../models/venues");
const multer = require("multer");

// Upload Image
const storage = multer.diskStorage({
    destination: "./repo/venue/",
    filename: function(req, file, cb) {
      cb(null, "venue-" + file.originalname);
    }
  });
  
const upload = multer({
    storage: storage,
    limits: { fileSize: 1000000 },
}).single("picture");


exports.get_all = function(req, res, next) {
    var {_id}= req.query;
    var query ={}
    if(_id){
        query = {...query, _id}
    }
    venues.find({}, function(err, data) {
        if (err){
            next(err);
        }
        else{
            res.json(data);
        }
    }).sort({name:1});
};

exports.create_data = function(req, res, next) {

    upload(req, res, err => {
        var new_data = {
            name: req.body.name,
            capasity: req.body.capasity,
            location : {
                latitude : req.body.latitude,
                longitude : req.body.longitude
            }
        };
        if (req.file != null) {
          Object.assign(new_data, { picture: req.file.filename });
        }
        var new_post = new venues(new_data);
        if (err) {
           next(err);
        } else {
            new_post.save(function(err, data) {
                if (err){
                    next(err);}
                else{
                    res.json(data);
                }
            });
        }
    });
};

exports.read_data = function(req, res, next) {
    venues.find({_id:req.params._id}, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};


exports.update_data = function(req, res, next) {
    upload(req, res, err => {
        var new_data = {
            name: req.body.name,
            capasity: req.body.capasity,
            location : {
                latitude : req.body.latitude,
                longitude : req.body.longitude
            }
        };
        if (req.file != null) {
            Object.assign(new_data, { picture: req.file.filename });
        }
        if (err) {
           next(err);
        } else {
           venues.findOneAndUpdate({_id: req.params._id}, new_data, {new: true}, function(err, data) {
              if (err){
                 next(err);}
              else{
                 res.json(data);
              }
           });
        }
    });
};

exports.delete_data = function(req, res, next) {
    venues.remove({
        _id: req.params._id
    }, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json({ message: 'successfully deleted', data : data });
        }
    });
};