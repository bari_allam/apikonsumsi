'use strict';
var entrie_officials = require("../models/entrie_officials");

exports.get_all = function(req, res, next) {
    entrie_officials.find({}, function(err, result) {
        if (err){
            next(err);
        }
        else{
            res.json(result);
        }
    });
};

exports.create_data = function(req, res, next) {
    var new_data = new entrie_officials(req.body);
    new_data.save(function(err, result) {
        if (err){
            next(err);}
        else{
        res.json(result);
        }
    });
};

exports.create_data_multiple = function(req, res, next) {
    entrie_officials.insertMany(req.body, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.read_data = function(req, res, next) {
    entrie_officials.find({_id:req.params._id}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.update_data = function(req, res, next) {
    entrie_officials.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.delete_data = function(req, res, next) {
    entrie_officials.remove({
        _id: req.params._id
    }, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json({ message: 'successfully deleted', result : result });
        }
    });
};