'use strict';
var entrie_schedules = require("../models/entrie_schedules");

exports.get_all = function(req, res, next) {
    entrie_schedules.find({}, function(err, result) {
        if (err){
            next(err);
        }
        else{
            res.json(result);
        }
    });
};

exports.read_data = function(req, res, next) {
    entrie_schedules.findOne({event_name:req.params.name}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.create_data = function(req, res, next) {
    var new_data = new entrie_schedules(req.body);
    new_data.save(function(err, result) {
        if (err){
            next(err);}
        else{
        res.json(result);
        }
    });
};

exports.update_data = function(req, res, next) {
    entrie_schedules.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};