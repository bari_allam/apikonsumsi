'use strict';
var participants = require("../models/participants");

const multer = require("multer");
// Upload Image
const storage = multer.diskStorage({
    destination: "./repo/participant",
    filename: function(req, file, cb) {
      cb(null, "participant-" + file.originalname +'-' + Date.now()+".jpg");
    }
  });
  
const upload = multer({
    storage: storage,
    //limits: { fileSize: 30000000 },
}).single("photo");

exports.get_all = function(req, res, next) {
    participants.find({}, function(err, result) {
        if (err){
            next(err);
        }
        else{
            res.json(result);
        }
    });
};

exports.get_data_by_contingent_type = function(req, res, next) {
    participants.find({contingent_id:req.params.contingent_id,event_id:req.params.event_id,participant_type:req.params.participant_type}, function(err, result) {
        if (err){
            next(err);
        }
        else{
            res.json(result);
        }
    });
};

exports.create_data = function(req, res, next) {
    var new_data = new participants(req.body);
    new_data.save(function(err, result) {
        if (err){
            next(err);}
        else{
        res.json(result);
        }
    });
};

exports.create_data_multiple = function(req, res, next) {
    participants.insertMany(req.body, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.upload_photo = function(req, res, next) {
    upload(req, res, err => {
        if (req.file != null) {
            res.json({ status: "success", photo: req.file.filename });
        }
        else{
            res.json({ status: "error" });
        }
    })
};

exports.read_data = function(req, res, next) {
    participants.find({_id:req.params._id}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.update_data = function(req, res, next) {
    participants.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.delete_data = function(req, res, next) {
    participants.remove({
        _id: req.params._id
    }, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json({ message: 'successfully deleted', result : result });
        }
    });
};