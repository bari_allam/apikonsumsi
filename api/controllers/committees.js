'use strict';
// var committees = require("../models/committees");
var committees = require("../models/consumption/committees");


exports.get_all = function(req, res, next) {
    var {_id}= req.query;
    var query ={}
    if(_id){
        query = {...query, _id}
    }
    committees.find(query, function(err, data) {
        if (err){
            next(err);
        }
        else{
            res.json(data);
        }
    });
};

exports.create_data = function(req, res, next) {
    var new_data = new committees(req.body);
    new_data.save(function(err, data) {
        if (err){
        next(err);}
        else{
        res.json(data);
        }
    });
};

exports.create_data_multiple = function(req, res, next) {
    committees.insertMany(req.body, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};

exports.read_data = function(req, res, next) {
    committees.find({_id:req.params._id}, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};


exports.update_data = function(req, res, next) {
    committees.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};

exports.delete_data = function(req, res, next) {
    committees.remove({
        _id: req.params._id
    }, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json({ message: 'successfully deleted', data : data });
        }
    });
};