const userModel = require('../models/users');
const bcrypt = require('bcryptjs'); 
const jwt = require('jsonwebtoken');
const sharp = require('sharp')
const multer = require("multer");
// Upload Image
const storage = multer.diskStorage({
    destination: "./repo",
    filename: function(req, file, cb) {
      cb(null, "user-" + file.originalname);
    }
  });
  
const upload = multer({
    storage: storage,
    limits: { fileSize: 1000000 },
}).single("photo");

module.exports.getAll = function(req,res,next){
   userModel.find({},function(err,result){
      if(err)
         next(err);
      else
          res.json(result);
   })
}

module.exports.create = function(req,res,next){
   upload(req, res, err => {
      var new_data = req.body;
      
      if (req.file != null) {
      //   Object.assign(new_data, { photo: req.file.filename });
         new_data.photo = req.file.filename
      }
      var new_data_user = new userModel(new_data);
      if (err) {
         next(err);
      } else {
         new_data_user.save(function(err, result) {
            if (err) 
               next(err);
            else
               res.json({status: "success", message: "User added successfully!!!", data: result});
         });
      }
    });
   
}
// Authentication + compareSync 
module.exports.authenticate = function(req,res,next){
   userModel.findOne({email:req.body.email}, function(err, userInfo){
      if (err) {
       next(err);
      } else {
          if(userInfo !== null){
          //if(userInfo.hasOwnProperty("password")){
            if(bcrypt.compareSync(req.body.password,  userInfo.password)) {
               const token = jwt.sign({id: userInfo._id}, req.app.get('secretKey'), { expiresIn: '1d' });
                 res.json({status:"success", message: "user found!!!", data:{user: userInfo, token:token}});
               }
               else{
                 res.json({status:"error", message: "Invalid password!!!", data:null});
               }
          }
          else{
            res.json({status:"error", message: "Invalid email!!!", data:null});
          }
      }
   });
}

exports.read_data = function(req, res, next) {
   userModel.find({_id:req.params._id}, function(err, data) {
       if (err){
           next(err);}
       else{
           res.json(data);
       }
   });
};


exports.update_data = function(req, res, next) {
   upload(req, res, err => {
      var new_data = req.body;
  
      if (req.file != null) {
        //Object.assign(new_data, { photo: req.file.filename });
        new_data.photo = req.file.filename
      }
      //var new_data_user = new userModel(new_data);
      if (err) {
         next(err);
      } else {
         userModel.findOneAndUpdate({_id: req.params._id}, new_data, {new: true}, function(err, data) {
            if (err){
               next(err);}
            else{
               res.json(data);
            }
         });
      }
    });
};

exports.delete_data = function(req, res, next) {
   userModel.remove({
       _id: req.params._id
   }, function(err, data) {
       if (err){
           next(err);}
       else{
           res.json({ message: 'successfully deleted', data : data });
       }
   });
};

