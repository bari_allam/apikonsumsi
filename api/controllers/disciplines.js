'use strict';
var disciplines = require("../models/disciplines");

exports.get_all = function(req, res, next) {
    var {_id}= req.query;
    var query ={}
    if(_id){
        query = {...query, _id}
    }
    disciplines.find(query, function(err, result) {
        if (err){
            next(err);
        }
        else{
            res.json(result);
        }
    });
};

exports.create_data = function(req, res, next) {
    var new_data = new disciplines(req.body);
    new_data.save(function(err, result) {
        if (err){
            next(err);}
        else{
        res.json(result);
        }
    });
};

exports.create_data_multiple = function(req, res, next) {
    disciplines.insertMany(req.body, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.read_data = function(req, res, next) {
    disciplines.find({_id:req.params._id}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.update_data = function(req, res, next) {
    disciplines.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.delete_data = function(req, res, next) {
    disciplines.remove({
        _id: req.params._id
    }, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json({ message: 'successfully deleted', result : result });
        }
    });
};