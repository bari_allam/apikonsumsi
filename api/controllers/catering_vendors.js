'use strict';
// var caterings = require("../models/catering_vendors");
var caterings = require("../models/consumption/catering_vendors");
const multer = require("multer");

// Upload Image
const storage = multer.diskStorage({
    destination: "./repo/caterings/",
    filename: function(req, file, cb) {
      cb(null, "user-" + file.originalname);
    }
  });

const foods = multer.diskStorage({
    destination: './repo/consumption/foods/',
    filename: function(req, file, cb) {
        cb(null, 'user-' + file.originalname);
    }
})
  
const upload = multer({
    storage: storage,
    foods: foods,
    limits: { fileSize: 1000000 },
}).single("logo");


exports.get_all = function(req, res, next) {
    var {_id}= req.query;
    var query ={}
    if(_id){
        query = {...query, _id}
    }
    caterings.find(query, function(err, data) {
        if (err){
            next(err);
        }
        else{
            res.json(data);
        }
    }).sort({name:1});
};

exports.create_data = function(req, res, next) {

    upload(req, res, err => {
        var new_data = {
            name: req.body.name,
            contact: req.body.contact
        };
        if (req.file != null) {
          Object.assign(new_data, { logo: req.file.filename });
        }
        var new_post = new caterings(new_data);
        if (err) {
           next(err);
        } else {
            new_post.save(function(err, data) {
                if (err){
                    next(err);}
                else{
                    res.json(data);
                }
            });
        }
    });
};

exports.create_data_multiple = function(req, res, next) {
    caterings.insertMany(req.body, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.read_data = function(req, res, next) {
    caterings.find({_id:req.params._id}, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};


exports.update_data = function(req, res, next) {
    upload(req, res, err => {
        var new_data = {
            name: req.body.name,
            contact: req.body.contact
        };
        if (req.file != null) {
            Object.assign(new_data, { logo: req.file.filename });
        }
        if (err) {
           next(err);
        } else {
           caterings.findOneAndUpdate({_id: req.params._id}, new_data, {new: true}, function(err, data) {
              if (err){
                 next(err);}
              else{
                 res.json(data);
              }
           });
        }
    });
};

exports.delete_data = function(req, res, next) {
    caterings.remove({
        _id: req.params._id
    }, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json({ message: 'successfully deleted', data : data });
        }
    });
};