'use strict';
var sports = require("../models/sports");
// var sports = require("../models/consumption/sports");

exports.get_all = function(req, res, next) {
    var {_id}= req.query;
    var query ={}
    if(_id){
        query = {...query, _id}
    }
    sports.find(query, function(err, data) {
        if (err){
            next(err);
        }
        else{
            res.json(data);
        }
    });
};

exports.create_data = function(req, res, next) {
    var new_data = new sports(req.body);
    new_data.save(function(err, data) {
        if (err){
        next(err);}
        else{
        res.json(data);
        }
    });
};

exports.create_data_multiple = function(req, res, next) {
    sports.insertMany(req.body, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};

exports.read_data = function(req, res, next) {
    sports.find({_id:req.params._id}, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};


exports.update_data = function(req, res, next) {
    sports.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};

exports.delete_data = function(req, res, next) {
    sports.remove({
        _id: req.params._id
    }, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json({ message: 'successfully deleted', data : data });
        }
    });
};