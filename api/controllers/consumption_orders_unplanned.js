'use strict';
// var consumption_orders_unplanned = require("../models/consumption_orders_unplanned");
var consumption_orders_unplanned = require("../models/consumption/consumption_orders_unplanned");

exports.get_all = function(req, res, next) {
    var {_id}= req.query;
    var query ={}
    if(_id){
        query = {...query, _id}
    }
    consumption_orders_unplanned.find(query, function(err, result) {
        if (err){
            next(err);
        }
        else{
            res.json(result);
        }
    });
};

exports.create_data = function(req, res, next) {
    var new_data = new consumption_orders_unplanned(req.body);
    new_data.save(function(err, result) {
        if (err){
            next(err);}
        else{
        res.json(result);
        }
    });
};

exports.create_data_multiple = function(req, res, next) {
    consumption_orders_unplanned.insertMany(req.body, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.read_data = function(req, res, next) {
    consumption_orders_unplanned.find({_id:req.params._id}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    })
    .populate('contingent')
    .populate('venue')
    .populate('sport')
    .populate('vendor')
    ;
};

exports.update_data = function(req, res, next) {
    consumption_orders_unplanned.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.delete_data = function(req, res, next) {
    consumption_orders_unplanned.remove({
        _id: req.params._id
    }, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json({ message: 'successfully deleted', result : result });
        }
    });
};


// Aggregate Here
exports.get_orders = function(req, res, next) {    
    consumption_orders_unplanned.aggregate([
        {
            "$addFields": {
                "consumption_orders_unplanned_id_ref":{
                    "$toString": "$_id"
                }
            }
        },
        {
            // Committees
            "$lookup": {
                "from": "committees",
                "localField": "consumption_orders_unplanned_id_ref",
                "foreignField": "_id",
                "as": "data_committees"
            },
            // Menu
            "$lookup": {
                "from": "catering_vendors",
                "localField": "consumption_orders_unplanned_id_ref",
                "foreignField": "menu.foods_name",
                "as": "data_menu"
            },
            // Destination
            "$lookup": {
                "from": "destination",
                "localField": "consumption_orders_unplanned_id_ref",
                "foreignField":"_id",
                "as": "data_destination"
            }
        },
        {
            "$sort": {
                "date": 1
            }
        }
    ], function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};