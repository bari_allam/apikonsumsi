'use strict';
// var consumption_orders_planned = require("../models/consumption_orders_planned");
var consumption_orders_planned = require("../models/consumption/consumption_orders_planned");

exports.get_all = function(req, res, next) {
    var {_id}= req.query;
    var query ={}
    if(_id){
        query = {...query, _id}
    }
    consumption_orders_planned.find(query, 
        function(err, result) {
        if (err){
            next(err);
        }
        else{
            res.json(result);
        }
    })
    // .populate('contingent')
    // .populate('venue')
    // .populate('sport')
    // .populate('event')
};

exports.create_data = function(req, res, next) {
    var new_data = new consumption_orders_planned(req.body);
    new_data.save(function(err, result) {
        if (err){
            next(err);}
        else{
        res.json(result);
        }
    })
};

exports.create_data_multiple = function(req, res, next) {
    consumption_orders_planned.insertMany(req.body, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.read_data = function(req, res, next) {
    consumption_orders_planned.find({_id:req.params._id}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    })
    .populate('contingent')
    .populate('venue')
    .populate('sport')
    .populate('vendor')
    ;
};

exports.update_data = function(req, res, next) {
    consumption_orders_planned.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json(result);
        }
    });
};

exports.delete_data = function(req, res, next) {
    consumption_orders_planned.remove({
        _id: req.params._id
    }, function(err, result) {
        if (err){
            next(err);}
        else{
            res.json({ message: 'successfully deleted', result : result });
        }
    });
};


// Aggregate Here
exports.create_aggregate = function(req, res, next) {    
    consumption_orders_planned.aggregate([
        {
            "$addFields": {
                "id_ref":{
                    "$toString": "$_id"
                }
            }
        },
        {
            // Contingents
            "$lookup": {
                "from": "contingents",
                "localField": "id_ref",
                // "foreignField": "name",
                "foreignField": "_id",
                "as": "data_contingents"
            },
            // Committees
            "$lookup": {
                "from": "committees",
                "localField": "id_ref",
                "foreignField": "name",
                "as": "data_committees"
            },
            // Menu
            "$lookup": {
                "from": "catering_vendors",
                "localField": "id_ref",
                "foreignField": "menu.foods_name",
                "as": "data_menu"
            },
            // Destination
            "$lookup": {
                "from": "destination",
                "localField": "id_ref",
                "foreignField":"name",
                "as": "data_destination"
            }
        },
        {
            "$sort": {
                "date": 1
            }
        }
    ], function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};