'use strict';
var events = require("../models/events");
const multer = require("multer");

// Upload Image
const storage = multer.diskStorage({
    destination: "./repo/sport/",
    filename: function(req, file, cb) {
      cb(null, "icon-" + file.originalname);
    }
  });
  
const upload = multer({
    storage: storage,
    limits: { fileSize: 1000000 },
}).single("event_icon");

exports.get_all = function(req, res, next) {
    events.find({}, function(err, data) {
        if (err){
            next(err);
        }
        else{
            res.json(data);
        }
    }).sort({event_name:1});
};

exports.create_data = function(req, res, next) {

    upload(req, res, err => {
        var new_data = {
            event_name: req.body.event_name,
            // sport_id : req.body.sport_id,
            // discipline_id : req.body.discipline_id,
            event_parent : 'PON XX 2020 Papua',
        };
        if (req.file != null) {
          Object.assign(new_data, { event_icon: req.file.filename });
        }
        var new_post = new events(new_data);
        if (err) {
           next(err);
        } else {
            new_post.save(function(err, data) {
                if (err){
                    next(err);}
                else{
                    res.json(data);
                }
            });
        }
    });
};

exports.read_data = function(req, res, next) {
    events.find({_id:req.params._id}, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};

exports.get_entrie_by_numbers = function(req, res, next) {    
    events.aggregate([
        {
            "$addFields": {
                "event_id_ref": {
                    "$toString": "$_id"
                }
            }
        },
        {
            "$lookup": {
                "from": "entrie_by_numbers",
                "localField": "event_id_ref",
                "foreignField": "event_id",
                "as": "data_entrie_by_numbers"
            }
        },
        {
            "$sort": {
                "event_name": 1
            }
        }
    ], function(err, data) {
        if (err){
            next(err);}
        else{
            res.json(data);
        }
    });
};

exports.update_data = function(req, res, next) {
    upload(req, res, err => {
        var new_data = {
            event_name: req.body.event_name,
        };
        if (req.file != null) {
            Object.assign(new_data, { event_icon: req.file.filename });
        }
        if (err) {
           next(err);
        } else {
            events.findOneAndUpdate({_id: req.params._id}, new_data, {new: true}, function(err, data) {
                if (err){
                    next(err);}
                else{
                    res.json(data);
                }
            });
        }})
};

exports.delete_data = function(req, res, next) {
    events.remove({
        _id: req.params._id
    }, function(err, data) {
        if (err){
            next(err);}
        else{
            res.json({ message: 'successfully deleted', data : data });
        }
    });
};