const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const mongoose = require('./jwt/config/database'); //database configuration
var jwt = require('jsonwebtoken');
const app = express();
app.set('secretKey', 'nodeRestApi'); // jwt secret token
// connection to mongodb
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));
app.use(logger('dev'));

// Body parser middleware
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//Cross Origin
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//repo image
app.use("/repo", express.static(path.join(__dirname, 'repo')));

app.get('/', function(req, res){
  res.json({"tutorial" : "Build REST API with node.js"});
});

//register router
const users = require('./api/routes/users');
const private_users = require('./api/routes/private/users');
const sports = require('./api/routes/sports');
const contingents = require('./api/routes/contingents');
// const private_contingents = require('./api/routes/private/contingents');
const disciplines = require('./api/routes/disciplines');
const events = require('./api/routes/events');
const private_events = require('./api/routes/private/events');
const entrie_schedules = require('./api/routes/entrie_schedules');
const venues = require('./api/routes/venues');
const private_venues = require('./api/routes/private/venues');
const private_entrie_by_numbers = require('./api/routes/private/entrie_by_numbers');
const private_participants = require('./api/routes/private/participants');
// Consumption
const catering_vendors = require('./api/routes/catering_vendors')
const committees = require('./api/routes/committees')
const consumption_orders_planned = require('./api/routes/consumption_orders_planned')
const consumption_orders_unplanned = require('./api/routes/consumption_orders_unplanned')
const destinations = require('./api/routes/destinations')
const hotel_vendors = require('./api/routes/hotel_vendors')
const registration_consumptions = require('./api/routes/registration_consumptions')


// public route
app.use('/users', users);
app.use('/sports', sports);
app.use('/contingents', contingents);
app.use('/events', events);
app.use('/disciplines', disciplines);
app.use('/entrie_schedules', entrie_schedules);
app.use('/venues', venues);
// Consmption
app.use('/catering_vendors', catering_vendors);
app.use('/committees', committees);
app.use('/consumption_orders_planned', consumption_orders_planned);
app.use('/consumption_orders_unplanned', consumption_orders_unplanned);
app.use('/destinations', destinations);
app.use('/hotel_vendors', hotel_vendors);
app.use('/registration_consumption', registration_consumptions);

// private route
app.use('/private/users',validateUser, private_users);
// app.use('/private/contingents',validateUser, private_contingents);
app.use('/private/events',validateUser, private_events);
app.use('/private/venues',validateUser, private_venues);
app.use('/private/entrie_by_numbers',validateUser, private_entrie_by_numbers);
app.use('/private/participants',validateUser, private_participants);
app.get('/favicon.ico', function(req, res) {
    res.sendStatus(204);
});

function validateUser(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) 
  {
    if (err) {
      res.json({status:"error", message: err.message, data:null});
    }
    else
    {
      // add user id to request
      req.body.userId = decoded.id;
      next();
    }
  });
}
// express doesn't consider not found 404 as an error so we need to handle 404 explicitly
// handle 404 error
app.use(function(req, res, next) {
 let err = new Error('Not Found');
    err.status = 404;
    next(err);
});
// handle errors
app.use(function(err, req, res, next) {
  console.log(err);
  if(err.status === 404)
    res.status(404).json({message: "Not found"});
  else 
    res.status(500).json({message: "Something looks wrong :( !!!"});
});

app.listen(200, function(){
  console.log('Node server listening on port 200');
});